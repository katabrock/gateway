package de.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatewayApplicationStarter {
    public static void main(String[] args) {
        SpringApplication.run(GatewayApplicationStarter.class, args);
    }
}
