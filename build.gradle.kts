import io.spring.gradle.dependencymanagement.dsl.DependencyManagementExtension

plugins {
    java
    id("org.springframework.boot") version "2.1.6.RELEASE"
    id("io.spring.dependency-management") version "1.0.8.RELEASE"
    id("nebula.release") version "11.0.0"
}

group = "de.claudio.nazareth"

repositories {
    jcenter()
    mavenCentral()
}

dependencies {
    implementation("org.springframework.cloud:spring-cloud-starter-gateway:2.1.2.RELEASE")
    implementation("org.springframework.boot:spring-boot-starter-logging")
    implementation("org.springframework.cloud:spring-cloud-starter-contract-stub-runner"){
        exclude(module = "spring-boot-starter-web")
    }
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(module = "junit")
        exclude(module = "org.hamcrest")
    }
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.4.2")
    implementation("org.springframework.boot:spring-boot-configuration-processor")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.4.2")
//    testImplementation("org.junit.platform:junit-platform-launcher:1.4.2")
    testImplementation("org.junit.platform:junit-platform-commons:1.4.2")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}

configure<DependencyManagementExtension> {
    imports {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:Greenwich.RELEASE")
    }
}

tasks.test {
    useJUnitPlatform()
}

nebulaRelease {

}